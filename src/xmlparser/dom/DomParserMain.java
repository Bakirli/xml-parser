package xmlparser.dom;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import xmlparser.domain.Person;

import javax.xml.parsers.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class DomParserMain {

	public static void main(String[] args) {

		try {

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();

			String file = "person.xml";

			Document document = builder.parse(file);
			document.normalizeDocument();

			NodeList personNodeList = document.getElementsByTagName("person");
			List<Person> personList = new ArrayList<>();

			for (int i = 0; i < personNodeList.getLength(); i++) {
				Node personNode = personNodeList.item(i);

				if (personNode.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) personNode;
					Person person = new Person();

					String id = element.getElementsByTagName("id").item(0).getTextContent();
					person.setId(Long.parseLong(id));

					String name = element.getElementsByTagName("name").item(0).getTextContent();
					person.setName(name);

					String surname = element.getElementsByTagName("surname").item(0).getTextContent();
					person.setSurname(surname);

					String salary = element.getElementsByTagName("salary").item(0).getTextContent();
					person.setSalary(new BigDecimal(salary));

					personList.add(person);
				}
			}

			System.out.println("list hazirdi");
			personList.forEach(System.out::println);


		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
	}
}
