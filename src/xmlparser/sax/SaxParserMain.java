package xmlparser.sax;

import org.xml.sax.SAXException;
import xmlparser.domain.Person;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.List;

public class SaxParserMain {

    public static void main(String[] args) {

        try {
            SaxContentHandler handler = new SaxContentHandler(3);

            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
//            String file = "person.xml";
            String file = "person2.xml";
            parser.parse(file, handler);

            System.out.println("person list hazirdi");
            List<Person> personList = handler.getPersonList();
            personList.forEach(System.out::println);

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
