package xmlparser.sax;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import xmlparser.domain.Person;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class SaxContentHandler extends DefaultHandler {

    private List<Person> personList;
    private Person tempPerson;
    private boolean isPerson;
    private boolean isId;
    private boolean isName;
    private boolean isSurname;
    private boolean isSalary;
    private int limit;
    private int counter;

    public SaxContentHandler(int limit) {
        this.personList = new ArrayList<>();
        this.isPerson = false;
        this.isId = false;
        this.isName = false;
        this.isSurname = false;
        this.isSalary = false;
        this.limit = limit;
        this.counter = 0;
    }

    @Override
    public void startDocument() throws SAXException {
        System.out.println("start document");

    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        System.out.println("start element " + qName);
        if(qName.equals("person")) {
            isPerson = true;
            tempPerson = new Person();
            long id = Long.parseLong(attributes.getValue("id"));
            tempPerson.setId(id);
            System.out.println("temp person = " + tempPerson);
        } else if(qName.equals("id")) {
            isId = true;
        }  else if(qName.equals("name")) {
            isName = true;
        } else if(qName.equals("surname")) {
            isSurname = true;
        } else if(qName.equals("salary")) {
            isSalary = true;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String data = new String(ch, start, length);
        System.out.println("data = " + data);
        printFlags();

        if(isId) {
            tempPerson.setId(Long.parseLong(data));
        } else if(isName) {
            tempPerson.setName(data);
        } else if(isSurname) {
            tempPerson.setSurname(data);
        } else if(isSalary) {
            tempPerson.setSalary(new BigDecimal(data));
        }

        System.out.println("temp person = " + tempPerson);

    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        System.out.println("end element " + qName);

        if(qName.equals("person")) {
            isPerson = false;

            if(counter < limit) {
                counter++;
                personList.add(tempPerson);
            } else {
                System.out.println("ignore person " + tempPerson);
            }
            tempPerson = null;
            System.out.println("person list = " + personList);
        } else if(qName.equals("id")) {
            isId = false;
        } else if(qName.equals("name")) {
            isName = false;
        } else if(qName.equals("surname")) {
            isSurname = false;
        } else if(qName.equals("salary")) {
            isSalary = false;
        }
    }

    @Override
    public void endDocument() throws SAXException {
        System.out.println("end document");
    }

    public List<Person> getPersonList() {
        return personList;
    }

    private void printFlags() {
        System.out.printf("person = %s, id = %s, name = %s, surname = %s, salary = %s\n", isPerson, isId, isName, isSurname, isSalary);
    }
}
