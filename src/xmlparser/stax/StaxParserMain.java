package xmlparser.stax;

import xmlparser.domain.Person;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class StaxParserMain {
    public static void main(String[] args) {
        XMLInputFactory factory = XMLInputFactory.newFactory();
        try {
//            String file = "person.xml";
            String file = "person2.xml";
            XMLStreamReader reader = factory.createXMLStreamReader(new FileInputStream(file));
            List<Person> personList = new ArrayList<>();
            Person tempPerson = null;
            boolean isPerson = false;
            boolean isId = false;
            boolean isName = false;
            boolean isSurname = false;
            boolean isSalary = false;
            int limit = 3;
            int counter = 0;

            while (reader.hasNext()) {
                int elementType = reader.next();

                if (elementType == XMLEvent.START_DOCUMENT) {
                    System.out.println("start document");

                } else if (elementType == XMLEvent.START_ELEMENT) {

                    System.out.println("start element " + reader.getName());
                    if (reader.getName().toString().equals("person")) {
                        isPerson = true;
                        tempPerson = new Person();
                        System.out.println("temp person = " + tempPerson);
                        long id = Long.parseLong(reader.getAttributeValue(null, "id"));
                        tempPerson.setId(id);

                    } else if (reader.getName().toString().equals("id")) {
                        isId = true;
                    } else if (reader.getName().toString().equals("name")) {
                        isName = true;
                    } else if (reader.getName().toString().equals("surname")) {
                        isSurname = true;
                    } else if (reader.getName().toString().equals("salary")) {
                        isSalary = true;
                    }

                } else if (elementType == XMLEvent.CHARACTERS) {
                    System.out.println("data " + reader.getText());

                   String data = reader.getText();

                    if(isId) {
                        tempPerson.setId(Long.parseLong(data));
                    } else if(isName) {
                        tempPerson.setName(data);
                    } else if(isSurname) {
                        tempPerson.setSurname(data);
                    } else if(isSalary) {
                        tempPerson.setSalary(new BigDecimal(data));
                    }

                    System.out.println("temp person = " + tempPerson);

                } else if (elementType == XMLEvent.END_ELEMENT) {
                    System.out.println("end element " + reader.getName());

                    if (reader.getName().toString().equals("person")) {
                        isPerson = false;

                        if(counter < limit) {
                            counter++;
                            personList.add(tempPerson);
                        } else {
                            break;
                        }
                        tempPerson = null;
                        System.out.println("person list = " + personList);
                    } else if (reader.getName().toString().equals("id")) {
                        isId = false;
                    } else if (reader.getName().toString().equals("name")) {
                        isName = false;
                    } else if (reader.getName().toString().equals("surname")) {
                        isSurname = false;
                    } else if (reader.getName().toString().equals("salary")) {
                        isSalary = false;
                    }

                } else if (elementType == XMLEvent.END_DOCUMENT) {
                    System.out.println("end document");
                }
            }

            System.out.println("person list hazirdi");
            personList.forEach(System.out::println);

        } catch (XMLStreamException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
