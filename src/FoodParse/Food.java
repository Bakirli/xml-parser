package FoodParse;

import java.math.BigDecimal;

public class Food {
	private String name;
	private BigDecimal price;
	private String description;
	private int calories;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getCalories() {
		return calories;
	}

	public void setCalories(int calories) {
		this.calories = calories;
	}

	@Override
	public String toString() {
		return "Food{" +
				"name='" + name + '\'' +
				", price=" + price +
				", description='" + description + '\'' +
				", calories=" + calories +
				'}';
	}
}
