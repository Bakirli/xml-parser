package FoodParse;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class FoodParserMain {

	public static void main(String[] args) {

		String file = "BreakfastMenu.xml";

		StaxParser parser = new StaxParser(file);
		parser.staxParse();

		System.out.println("list hazirdi");
		System.out.println("----------------");
		parser.getFoodList().forEach(System.out::println);
	}
}
