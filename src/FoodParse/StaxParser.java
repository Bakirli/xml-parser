package FoodParse;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class StaxParser {

	private String file;
	private List<Food> foodList;

	public StaxParser(String file) {
		this.file = file;
	}

	public void staxParse() {
		try {
			XMLInputFactory factory = XMLInputFactory.newInstance();
//			String file = "BreakfastMenu.xml";
			XMLStreamReader reader = factory.createXMLStreamReader(new FileInputStream(file));

			foodList = new ArrayList<>();
			Food tempFood = null;
			boolean isFood = false;
			boolean isName = false;
			boolean isPrice = false;
			boolean isDescription = false;
			boolean isCalories = false;

			while (reader.hasNext()) {
				int count = reader.next();


				if (count == XMLEvent.START_DOCUMENT) {
					System.out.println("start document");

				} else if (count == XMLEvent.START_ELEMENT) {
					System.out.println("start element = " + reader.getName());
					if (reader.getName().toString().equals("food")) {
						isFood = true;
						tempFood = new Food();

					} else if (reader.getName().toString().equals("name")) {
						isName = true;

					} else if (reader.getName().toString().equals("price")) {
						isPrice = true;

					} else if (reader.getName().toString().equals("description")) {
						isDescription = true;

					} else if (reader.getName().toString().equals("calories")) {
						isCalories = true;

					}

				} else if (count == XMLEvent.CHARACTERS) {
					String data = reader.getText();
					System.out.println("data = " + data);

					if (isName) {
						tempFood.setName(data);
					} else if (isPrice) {
						if(data.contains("$")) {
							data = data.replace("$", "");
							tempFood.setPrice(new BigDecimal(data));
						}
					}else if (isDescription) {
						tempFood.setDescription(data);
					} else if (isCalories) {
						tempFood.setCalories(Integer.parseInt(data));
					}

				} else if (count == XMLEvent.END_ELEMENT) {

					System.out.println("end element = " + reader.getName());
					if (reader.getName().toString().equals("food")) {
						isFood = false;
						foodList.add(tempFood);
//						tempFood = null;

					} else if (reader.getName().toString().equals("name")) {
						isName = false;

					} else if (reader.getName().toString().equals("price")) {
						isPrice = false;

					} else if (reader.getName().toString().equals("description")) {
						isDescription = false;

					} else if (reader.getName().toString().equals("calories")) {
						isCalories = false;

					}

				} else if (count == XMLEvent.END_DOCUMENT) {
					System.out.println("end document");

				}
			}

//			System.out.println("list hazirdi");
//			foodList.forEach(System.out::println);

		} catch (XMLStreamException | FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public List<Food> getFoodList() {
		return foodList;
	}
}
